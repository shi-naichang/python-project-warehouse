# models.py
# 映射数据库中的字段

from django.db import models


# Create your models here.
class User(models.Model):
    username = models.CharField(verbose_name="用户名", max_length=20)
    password = models.CharField(verbose_name="密码 ", max_length=20)

    def __str__(self):
        return f'{self.username} - {self.password}'


class Group(models.Model):
    name = models.CharField(verbose_name="社团名称", max_length=20)
    description = models.CharField(verbose_name="社团描述", max_length=255)
    user = models.CharField(verbose_name="社团负责人", max_length=20)
    num = models.IntegerField(verbose_name="人数", default=0)

    def __str__(self):
        return f'{self.name} - {self.description} - {self.user} - {self.num}'


class Student(models.Model):
    student_id = models.CharField(verbose_name="学号", max_length=20)
    student_name = models.CharField(verbose_name="姓名", max_length=20)
    grade = models.CharField(verbose_name="年级", max_length=20)
    major = models.CharField(verbose_name="专业", max_length=20)
    phone = models.CharField(verbose_name="电话", max_length=20)
    group = models.ForeignKey(Group, verbose_name="所在社团", on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.student_id} - {self.student_name} - {self.grade} - {self.major} - {self.phone} - {self.group}'
