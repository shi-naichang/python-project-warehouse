# group.py

import pandas as pd
from django.shortcuts import render, redirect

from web import models


def group(request):
    # 返回group页面
    if request.method == 'GET':
        # 获取到全都的社团信息表
        res = models.Group.objects.all()
        return render(request, 'group.html', {"res": res})


# 查询社团信息的方法
def selectGroup(request):
    if request.method == 'GET':
        # 首先获取前端返回来的社团name 其次根据name去数据库查询
        name = request.GET.get('group')
        print(name)
        # 获取到name查询出来的信息
        res = models.Group.objects.filter(name=name).first()
        # 如果是res里面有参数的话 将参数返回给页面
        if res:
            stu_res = models.Student.objects.filter(student_name=res.user).first()
            return render(request, 'detail-group.html', {'res': res, 'stu_res': stu_res})
        else:
            return render(request, 'detail-group.html', {'msg': '没有找到该社团'})


# 增加社团信息的方法
def addGroup(request):
    if request.method == 'POST':
        # 首先获取前端传来的信息
        name = request.POST.get('name')
        description = request.POST.get('description')
        username = request.POST.get('username')
        userid = request.POST.get('userid')
        usergrade = request.POST.get('usergrade')
        usermajor = request.POST.get('usermajor')
        userphone = request.POST.get('userphone')
        # 判断前端传来的信息是否为空
        if name and description and username and userid and usergrade and usermajor and userphone:
            # 将社团信息插入到数据库中
            models.Group.objects.create(name=name, description=description, user=username, num=1)
            # 获取到社团信息的id
            res_id = models.Group.objects.filter(name=name).first().id
            # 将社团信息的id 插入到student表中
            models.Student.objects.create(student_name=username, student_id=userid, grade=usergrade, major=usermajor,
                                          phone=userphone, group_id=res_id)
            return render(request, 'error.html', {'msg': name + '添加成功！！！'})
        else:
            return render(request, 'error.html', {'msg': '请填写完整信息！！！'})


def excelGroup(request):
    # 首先获取到所有的信息
    res = models.Group.objects.all()
    if not res:
        # 返回错误信息
        return render(request, 'error.html', {'msg': '没有查询到社团信息'})

    # 创建空的 DataFrame，设置字段
    columns = ['社团名称', '社团描述', '社团管理者', '总人数']
    df = pd.DataFrame(columns=columns)

    # 遍历查询到的信息
    for item in res:
        # 获取到社团信息
        dic = dict()
        dic = {
            '社团名称': item.name,
            '社团描述': item.description,
            '社团管理者': item.user,
            '总人数': item.num
        }
        # 逐行插入数据
        df = pd.concat([df, pd.DataFrame([dic])], ignore_index=True)
    # 导出到本地
    df.to_excel('./file/group.xlsx', index=False)
    # 设置导出成功的信息
    return render(request, 'error.html', {'msg': '社团信息表导出成功'})


def deleteGroup(request):
    gid = request.GET.get('gid')
    res = models.Group.objects.filter(id=gid).first()
    # 判断是否查询到信息
    if not res:
        return render(request, 'error.html', {'msg': '没有查询到社团信息'})
    # 删除社团
    models.Group.objects.filter(id=gid).delete()
    sres = models.Student.objects.filter(group_id=gid)
    for item in sres:
        # 获取查询信息中的id
        sid = item.id
        # 根据id进行删除
        models.Student.objects.filter(id=sid).delete()
    return render(request, 'error.html', {'msg': res.name + '已解散'})


def updateGroup(request):
    if request.method == 'GET':
        # 获取前端传来的字段
        name = request.GET.get('name')
        description = request.GET.get('description')
        user = request.GET.get('user')
        if name and description and user:
            # 判断是否查询到信息 根据社团名称查询
            res = models.Group.objects.filter(name=name).first()
            if not res:
                return render(request, 'error.html', {'msg': '没有查询到社团信息,社团名不存在'})
            #  判断是否查询到信息 根据社团负责人查询
            res = models.Student.objects.filter(student_name=user).first()
            if not res:
                return render(request, 'error.html', {'msg': '社团负责人不存在'})
            models.Group.objects.filter(name=name).update(description=description, user=user)
            return render(request, 'error.html', {'msg': '修改成功'})
        else:
            return render(request, 'error.html', {'msg': '修改失败,请检查是否填写完整'})
