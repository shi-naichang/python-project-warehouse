from web import models
import pandas as pd


class Table:
    def __init__(self, data):
        self.data = data

    # 将data导出到学生excel表中
    def studentToExcel(self):
        df = pd.DataFrame(columns=['姓名', '学号', '年级', '专业', '联系方式', '所在社团'])
        res = self.data
        for item in res:
            gid = item.group_id
            group = models.Group.objects.filter(id=gid).first().name
            df = df.append({'姓名': item.student_name, '学号': item.student_id, '年级': item.grade, '专业':
                            item.major, '联系方式': item.phone, '所在社团': group}, ignore_index=True)
        df.to_excel('web/file/student.xlsx', index=False, mode='w')

    # 将data导出到社团excel表中
    def groupToExcel(self):
        df = pd.DataFrame(columns=['社团名称', '社团描述', '社团负责人', '总人数'])
        res = self.data
        for item in res:
            df = df.append({'社团名称': item.name, '社团描述': item.description, '社团负责人': item.user, '总人数': item.num}, ignore_index=True)
        df.to_excel('web/file/group.xlsx', index=False, mode='w')

    # 根据id修改数据库中的字段
    def updateGroupById(self, info):
        gid = self.data.group_id
        models.Group.objects.filter(id=gid).update(**info)

    # 根据id修改数据库中的字段
    def updateStudentById(self, info):
        gid = self.data.id
        models.Student.objects.filter(id=gid).update(**info)
