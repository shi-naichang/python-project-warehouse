#login.py

from django.shortcuts import render, redirect
from web import models
# Create your views here.
# 登录视图
def login(request):
    # 如果是get请求 就继续返回login页面
    if request.method == 'GET':
        return render(request, 'login.html')
    # 否则的话获取login页面用户填写的user和pwd，去数据库进行校验
    else:
        # 获取前端返回的user和pwd
        username = request.POST.get('user')
        password = request.POST.get('pwd')
        if not password:
            # 如果密码为空 返回login页面 并提示密码不能为空
            return render(request, 'login.html', {"msg": "密码不能为空"})
        if not username:
            #  如果用户名为空 返回login页面 并提示用户名不能为空
            return render(request, 'login.html', {"msg": "用户名不能为空"})
        # 根据用户名去数据库查询
        res = models.User.objects.filter(username=username).first()
        if not res:
            return render(request, 'login.html', {"msg": "用户名不存在"})
        if res.password == password:
            return render(request, 'index.html', {'msg': username + "登录成功"})
        else:
            return render(request, 'login.html', {"msg": "密码错误"})
