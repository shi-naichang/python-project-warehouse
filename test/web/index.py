from django.shortcuts import render, redirect

from web import models


def index(request):
    # 返回index首页
    return render(request, 'index.html')
