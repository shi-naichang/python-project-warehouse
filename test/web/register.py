#register.py

from django.shortcuts import render, redirect

from web import models


def register(request):
    # 如果是post请求 获取前端返回的user和pwd，去数据库进行校验
    if request.method == 'POST':
        # 获取前端返回的user，pwd，rpwd
        username = request.POST.get('user')
        password = request.POST.get('pwd')
        rpassword = request.POST.get('rpwd')
        # 判断是否填写
        if not username or not password or not rpassword:
            return render(request, 'register.html', {"msg": "必须填写所有信息"})

        # 如果两次的密码不一致，则返回msg给register页面
        if password != rpassword:
            return render(request, 'register.html', {"msg": "两次密码不一致"})
        # 根据用户名查询用户名是否重复
        res = models.User.objects.filter(username=username)
        # 如果不重复，则返回msg给register页面
        if not res:
            #  如果用户名不存在，则创建用户
            models.User.objects.create(username=username, password=password)
            return render(request, 'login.html', {'msg': username + '注册成功'})
        else:
            #  如果用户名存在，则返回msg给register页面
            return render(request, 'register.html', {"msg": "用户名已存在"})
    else:
        # get请求返回register页面
        return render(request, 'register.html')
