#student.py
from django.db.models import F
from django.shortcuts import render
import pandas as pd
from web import models


def student(request):
    # 返回student页面
    if request.method == 'GET':
        return render(request, 'student.html')


# 查询数据库中的学生的信息
def selectStudent(request):
    if request.method == 'GET':
        # 获取前端返回的值
        name = request.GET.get('sname')
        sid = request.GET.get('sid')
        if not name and not sid:
            # 如果没有输入查询条件，则返回错误信息
            return render(request, 'error.html', {'msg': '请输入查询条件'})
        else:
            print(name, sid)
            if name and sid:
                #  如果有输入了两个条件，则返回查询结果
                res = models.Student.objects.filter(student_name=name, student_id=sid).first()
            elif name:
                # 如果是输入了学生姓名，则返回查询结果
                res = models.Student.objects.filter(student_name=name).first()
            else:
                #  如果是输入了学生学号，则返回查询结果
                res = models.Student.objects.filter(student_id=sid).first()
            if res:
                # 首先取出学生的id
                gid = res.group_id
                # 根据id去group表里查询社团名称
                name = models.Group.objects.filter(id=gid).first().name
                return render(request, 'detail-student.html', {'res': res, 'name': name})
            else:
                return render(request, 'error.html', {'msg': '没有查询到学生信息'})


def addStudent(request):
    if request.method == 'POST':
        # 首先取出前端传来的参数
        sname = request.POST.get('stuname')
        sid = request.POST.get('stuid')
        grade = request.POST.get('grade')
        major = request.POST.get('major')
        phone = request.POST.get('phone')
        group = request.POST.get('group')
        if sname and sid and grade and major and phone and group:
            # 去数据库查询学号是否重复
            res = models.Student.objects.filter(student_id=sid)
            if res:
                return render(request, 'error.html', {'msg': '学号重复'})
            # 去数据库查询社团是否存在
            res = models.Group.objects.filter(name=group)
            if not res:
                return render(request, 'error.html', {'msg': '社团不存在'})
            # 根据前端返回的group查询group表中的id
            gid = models.Group.objects.filter(name=group).first().id
            # 将信息插入到student表中
            models.Student.objects.create(student_name=sname, student_id=sid, grade=grade, major=major,
                                          phone=phone, group_id=gid)
            # 将社团中的人数自增1
            models.Group.objects.filter(id=gid).update(num=F('num') + 1)
            # 返回成功的信息给前端页面
            return render(request, 'error.html', {'msg': '添加学生成功'})
        else:
            return render(request, 'error.html', {'msg': '添加学生失败,请填写完整!!!'})


def excelStudent(request):
    # 首先获取到所有的信息
    res = models.Student.objects.all()
    if not res:
        # 返回错误信息
        return render(request, 'error.html', {'msg': '没有查询到学生信息'})
    # 创建空的 DataFrame，设置字段
    columns = ['学号', '姓名', '年级', '专业', '手机号', '所在社团']
    df = pd.DataFrame(columns=columns)

    for item in res:
        # 获取社团的名称
        gid = item.group_id
        name = models.Group.objects.filter(id=gid).first().name
        # 封装成字典的形式
        dic = dict()
        dic = {
            '学号': item.student_id,
            '姓名': item.student_name,
            '年级': item.grade,
            '专业': item.major,
            '手机号': item.phone,
            '所在社团': name
        }
        # 逐行插入数据
        df = pd.concat([df, pd.DataFrame([dic])], ignore_index=True)
    df.to_excel('./file/student.xlsx', index=False)  # 将数据写入 Excel 文件
    # 返回成功信息
    return render(request, 'error.html', {'msg': '学生信息表导出成功'})


def deleteStudent(request):
    sid = request.GET.get('sid')
    print(sid)
    # 判断学生是否存在
    if not sid:
        return render(request, 'error.html', {'msg': '学生不存在'})
    # 判断一下学生是否为社团管理员
    sres = models.Student.objects.filter(student_id=sid).first()  # 获取该学生参加的社团信息
    gid = sres.group_id  # 获取学生参加的社团id
    gres = models.Group.objects.filter(id=gid).first()  # 获取社团信息
    if gres.user == sres.student_name:
        return render(request, 'error.html', {'msg': '该学生是社团管理员，不能删除'})
    # 删除该学生的信息
    models.Student.objects.filter(student_id=sid).delete()
    # 修改社团中的总人数
    models.Group.objects.filter(id=gid).update(num=F('num') - 1)
    return render(request, 'error.html', {'msg': sres.student_name + '删除成功'})


def updateStudent(request):
    # 获取前端传递的数据
    student_name = request.GET.get('stuname')
    student_id = request.GET.get('stuid')
    grade = request.GET.get('grade')
    major = request.GET.get('major')
    phone = request.GET.get('phone')
    group_name = request.GET.get('group')
    print(student_name, student_id, grade, major, phone, group_name)
    #  判断数据是否为空
    if student_name and student_id and grade and major and phone and group_name:
        gres = models.Group.objects.filter(name=group_name).first()
        #  判断该社团是否存在
        if not gres:
            return render(request, 'error.html', {'msg': '该社团不存在'})
        # 获取原来的信息
        sres = models.Student.objects.filter(student_id=student_id).first()
        if not sres:
            return render(request, 'error.html', {'msg': '该学生不存在,学号不存在'})
        # 将新加入的社团的总人数+1
        models.Group.objects.filter(id=gres.id).update(num=F('num') + 1)
        # 将原来社团的总人数-1
        old_group = models.Group.objects.filter(id=sres.group_id).first()
        # 更新学生的信息
        models.Group.objects.filter(name=old_group.name).update(num=F('num') - 1)
        models.Student.objects.filter(student_id=student_id).update(student_name=student_name,
                                                                    grade=grade,
                                                                    major=major,
                                                                    phone=phone,
                                                                    group_id=gres.id)
        return render(request, 'error.html', {'msg': '更新完成'})
    else:
        return render(request, 'error.html', {'msg': '请输入完整信息'})
