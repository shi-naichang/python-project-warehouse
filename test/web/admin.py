from django.contrib import admin
from web.models import *

# # Register your models here.
admin.site.register(User)
admin.site.register(Group)
admin.site.register(Student)
