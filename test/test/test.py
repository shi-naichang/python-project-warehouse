import pandas as pd

# 创建空的 DataFrame，设置字段
columns = ['序号', '人员', '地址', '年龄', '月薪']
df = pd.DataFrame(columns=columns)

# 待插入的数据
data_to_insert = [
    {'序号': 1, '人员': '小明', '地址': '北京', '年龄': 25, '月薪': 3000},
    {'序号': 2, '人员': '小红', '地址': '上海', '年龄': 30, '月薪': 4000},
    {'序号': 3, '人员': '小刚', '地址': '广州', '年龄': 35, '月薪': 5000}
]

# 逐行插入数据
for row_data in data_to_insert:
    df = pd.concat([df, pd.DataFrame([row_data])], ignore_index=True)

# 将数据写入 Excel 文件
df.to_excel('output.xlsx', index=False)
